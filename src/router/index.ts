import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Detail from '../views/Detail.vue'
import Categories from '../views/Categories.vue'
import Random from '../views/Randomizer.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail,
    props: {
      default: true
    }
  },
  {
    path: '/categories',
    name: 'categories',
    component: Categories
  },
  {
    path: '/randomizer',
    name: 'randomizer',
    component: Random
  }
]

const router = new VueRouter({
  routes
})
//Go directly to the home page
//router.replace('/home')

export default router
