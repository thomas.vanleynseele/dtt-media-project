/**
 * The restclient retrieves the data from our public api
 */
import axios from 'axios'

const RESOURCE_PATH = 'https://programming-quotes-api.herokuapp.com/quotes/'

export default class Restclient {

    //Get all quotes(~500 quotes)
    async getAllQuotes () {
        const { data } = await axios.get(RESOURCE_PATH)
        return data
    }
    //Get one page of quotes(20 quotes)
    async getOnePageOfQuotes () {
        const { data } = await axios.get(RESOURCE_PATH + 'page/7')
        return data
    }
    //Get one quote by his id
    async getById (id: String) {
        const { data } = await axios.get(RESOURCE_PATH + '/id/' + id)
        return data
    }
    //Get one random quote
    async getRandom () {
        const { data } = await axios.get(RESOURCE_PATH + 'random')
        return data
    }
}