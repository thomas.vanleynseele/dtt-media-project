module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },

  'extends': [
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/typescript'
  ],

  // required to lint *.vue files
  plugins: [
    'vue'
  ],

  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow paren-less arrow functions
    'arrow-parens': 'off',
    'one-var': 'off',
    'quotes': ['error', 'single'],
    'semi': ['error', 'never', { 'beforeStatementContinuationChars': 'always' }],
    'semi': [2, 'never'],

    // allow debugger during development only
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },

  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  }
}
